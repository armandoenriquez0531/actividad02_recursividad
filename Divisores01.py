'''
Created on 8 oct. 2021

@author: arman
'''
class DivisoresRecursivos:
    
    def divisores(self,num,a):
        if(num>1):
            if(num%a==0):
                print(a)
                DivisoresRecursivos.divisores(self, num, a+1)
            elif ((a+1)<=num):
                DivisoresRecursivos.divisores(self, num, a+1)
        if(num==1):
            print(num)


a=DivisoresRecursivos()

try:
    numero=int(input("Ingresa el numero para obtener sus divisores: "))
    print("Los divisores son: ")
    a.divisores(numero,2)
except ValueError:
    print("No ingresaste un valor numerico entero")

