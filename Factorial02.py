'''
Created on 8 oct. 2021

@author: arman
'''
class FactorialRecursivo:
    
    def calculoFactorial(self,numero):
        if(numero<=1):
            return 1
        else:
            return numero*FactorialRecursivo.calculoFactorial(self, numero-1)
            
        pass

a=FactorialRecursivo()
try:
    numero=int(input("Ingresa el numero para obtener su factorial: "))
    print("Su factorial es: ",a.calculoFactorial(numero))
except ValueError:
    print("No ingresaste un valor numerico entero")